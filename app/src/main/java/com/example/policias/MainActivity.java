package com.example.policias;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    private static final int REQUEST_CAMERA = 152;

    private Button iniciar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            // Explicamos porque necesitamos el permiso
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.CAMERA)) {

            }
            else {

                // El usuario no necesitas explicación, puedes solicitar el permiso:
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA},
                        REQUEST_CAMERA);

                //
            }
        }

        llenarBaseDatos();

        iniciar = findViewById(R.id.iniciar);

//        Metodo para lanzar activity de escaner
        iniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplication(), Escaner.class);
                startActivity(intent);
            }
        });
    }

    private void llenarBaseDatos() {
        //        Rutina que llena la base de datos, no es necesario volver a ejecutar a menos que la bd se actualice
        try {
            DBHelper helper = new DBHelper(this, "policia", null, 1);
            SQLiteDatabase database = helper.getWritableDatabase();

            if(database != null) {
                database.execSQL("INSERT INTO question VALUES(1 , '¿Sus ojos se ponen rojos con facilidad?')");
                database.execSQL("INSERT INTO question VALUES(2 , '¿Sufre algún trastorno que lo haga temblar?')");
                database.execSQL("INSERT INTO question VALUES(3 , '¿Siempre tiene un comportamiento agresivo?')");
                database.execSQL("INSERT INTO question VALUES(4 , '¿Está dispuesto a realizarse un examen de sangre?')");
                database.execSQL("INSERT INTO question VALUES(5 , '¿Le es dificil sentir dolor físico?')");
                database.execSQL("INSERT INTO question VALUES(6 , '¿Le gusta utilizar mucho desodorante o trata de ocultar algún aroma?')");
                database.execSQL("INSERT INTO question VALUES(7 , '¿Ha manejado bajo los efectos del alcohol?')");
                database.execSQL("INSERT INTO question VALUES(8 , '¿Cree que ah expuesto su vida el dia de hoy?')");
                database.execSQL("INSERT INTO question VALUES(9 , '¿Expuso la vida de alguien más el dia de hoy?')");
                database.execSQL("INSERT INTO question VALUES(10 , '¿Admite que la información que ah proporcionado es cierta?')");
            }
        }
        catch (Exception e) {
            Log.e("err", e.toString());
        }
    }
}
