package com.example.policias;

import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.affectiva.android.affdex.sdk.Frame;
import com.affectiva.android.affdex.sdk.detector.CameraDetector;
import com.affectiva.android.affdex.sdk.detector.Detector;
import com.affectiva.android.affdex.sdk.detector.Face;

import java.util.ArrayList;
import java.util.List;

public class Escaner extends AppCompatActivity implements Detector.ImageListener{
    private SurfaceView surface;
    private double enojo;
    private TextView question;
    private double disgusto;
    private CameraDetector detector;
    private int cont = 1;
    private double miedo;
    private Button next;
    private String preg;
    private List<Resultados> results;
    private TextView resultados;
    private LinearLayout pregs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_escaner);

        surface = findViewById(R.id.surface);
        question = findViewById(R.id.question);
        results = new ArrayList<>();
        resultados = findViewById(R.id.results);
        pregs = findViewById(R.id.pregs);

        next = findViewById(R.id.next);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                results.add(new Resultados(preg, enojo, disgusto, miedo));

                if(cont <= 10) {
                    getPregunta();
                    cont++;
                }
                else {
                    pregs.setVisibility(View.GONE);
                    resultados.setText(initAnalisis());
                }

            }
        });


        detector = new CameraDetector(this, CameraDetector.CameraType.CAMERA_FRONT, surface);
        detector.setImageListener(this);
        detector.setDetectAnger(true);
        detector.setDetectDisgust(true);
        detector.setDetectFear(true);
        detector.setMaxProcessRate(10);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;
        ViewGroup.LayoutParams params = surface.getLayoutParams();
        params.width = (int) (width * 0.45);
        params.height = (int) (height * 0.40);
        surface.setLayoutParams(params);

        getPregunta();
        cont++;
    }

    private void getPregunta() {
        try {
            DBHelper helper = new DBHelper(this, "policia", null, 1);
            SQLiteDatabase database = helper.getReadableDatabase();

            if(database != null) {
                Cursor cursor = database.rawQuery("SELECT * FROM question WHERE id=" + cont,null);
                if(cursor.moveToFirst()) {
                    question.setText(cursor.getInt(0) + ".- " + cursor.getString(1));
                    preg = cursor.getString(1);
                }
            }
        }
        catch (Exception e) {
            Log.e("err", e.toString());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            detector.start();
        }
        catch (Exception e) {
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        detector.stop();
    }

    @Override
    public void onImageResults(List<Face> list, Frame frame, float v) {
        if(list.size() > 0) {
            Face face = list.get(0);

            enojo = face.emotions.getAnger();
            disgusto = face.emotions.getDisgust();
            miedo = face.emotions.getFear();

        }
    }

    private String initAnalisis() {
        int porcentaje = 0;
        String estado = "";
        int prob = 0;

        for(int i = 0;i < results.size();i++) {
            if(results.get(i).getEnojo() > 50)
                porcentaje += 30;
            if(results.get(i).getDisgusto() > 30)
                porcentaje += 20;
            if(results.get(i).getMiedo() > 10)
                porcentaje += 30;

            prob += results.get(i).getEnojo() + results.get(i).getMiedo();

            if(porcentaje >= 30) {
                estado += results.get(i).getPregunta() + "\n" +
                        "Miente\n\n";
            }
            else {
                estado += results.get(i).getPregunta() + "\n" +
                        "No miente\n\n";
            }

            porcentaje = 0;
        }

        if(prob > 80)
            estado += "Parece que esta bajo influencia de estupefacientes";
        else
            estado += "No presenta apariencia anormal";

        return estado;
    }
}
